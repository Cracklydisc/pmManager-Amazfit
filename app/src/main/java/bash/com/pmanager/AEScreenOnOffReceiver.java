package bash.com.pmanager;

/**
 * Created by BASH on 23/02/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

public class AEScreenOnOffReceiver extends BroadcastReceiver {

    private boolean screenOff;


    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "BroadcastReceiver", Toast.LENGTH_SHORT).show();

        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {

            screenOff = true;

        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {

            screenOff = false;

        }


        if(screenOff) {
            Intent i = new Intent(context, AEScreenOnOffService.class);
            i.putExtra("screen_off", screenOff);
            context.startService(i);
            Log.i("ScreenOnOff", "Receive info...");
        }
        //Toast.makeText(this, "Receive info", Toast.LENGTH_SHORT).show();
    }

}