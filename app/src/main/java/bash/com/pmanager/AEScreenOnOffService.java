package bash.com.pmanager;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;


public class AEScreenOnOffService extends Service {
    BroadcastReceiver mReceiver=null;
    private ArrayList<String> mList;

    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
       // PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
       // PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
       // wl.acquire();
        //Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

       // boolean screenOff = intent.getBooleanExtra("screen_off", false);

        Toast.makeText(this, "Service Started: ", Toast.LENGTH_LONG).show();

      /*  if(screenOff) {
            Intent i = new Intent(this, LockScreenActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }*/

        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
                wl.acquire();

                Intent i = new Intent(AEScreenOnOffService.this, LockScreenActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        }).start();
        return START_REDELIVER_INTENT;
        //return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();


      //  PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
       // PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        //wl.acquire();

        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        mReceiver = new AEScreenOnOffReceiver();
        registerReceiver(mReceiver, filter);


    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        Toast.makeText(getBaseContext(), "Service is running", Toast.LENGTH_SHORT).show();
        return null;
    }

    @Override
    public void onDestroy() {

        Log.i("ScreenOnOff", "Service  destroy");
        if(mReceiver!=null)
            unregisterReceiver(mReceiver);

    }
}