package bash.com.pmanager;

/* CREDIT to penguinunix [forum.xda-developers.com] don't mind to keep this line won't you :D */

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "pmanagerPref";
    public static final String mBroadcastStringAction = "ACTION_1";
    public static final String  mBroadcastIntegerAction = "ACTION_2";

    private boolean mainEnable = false;

    private TextView mTextView;
    private Switch mainSwitch;
    private Intent screenOffIntent;
    private SharedPreferences settings;
    private boolean bootStart = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settings = getSharedPreferences(PREFS_NAME, 0);
        mainEnable = settings.getBoolean("disable_loockscreen", false);

        Intent myIntent = getIntent();
        bootStart = myIntent.getBooleanExtra("boot_start", false);

        Log.w("boot_test", "onCreate: " + bootStart);

       // IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
      //  filter.addAction(Intent.ACTION_SCREEN_OFF);
        //BroadcastReceiver mReceiver = new AEScreenOnOffReceiver();
       // registerReceiver(mReceiver, filter);

        if(!bootStart)
            ManagedApp();

       // screenOffIntent = new Intent(this, AEScreenOnOffService.class);
        StartOrStop();
    }

    private void Enable() {
        startService(new Intent(this, AEScreenOnOffService.class));
    }

    private void Disable() {
        stopService(new Intent(this, AEScreenOnOffService.class));
    }

    private void ManagedApp() {
        setContentView(R.layout.activity_main);
        mainSwitch = (Switch) findViewById(R.id.switchMain);
        mainSwitch.setChecked(mainEnable);

        mainSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("disable_loockscreen", isChecked);
                editor.commit();
                mainEnable = settings.getBoolean("disable_loockscreen", false);

                StartOrStop();
            }
        });
    }

    private void StartOrStop(){
        if(mainEnable){
            Enable();
            return;
        }

        Disable();

    }
}
